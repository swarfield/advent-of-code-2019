# https://adventofcode.com/2019/day/2

from copy import deepcopy
from functools import reduce

class Int_Code:
    def __init__(self):
        self.inst_ptr = 0 # Instruction Pointer
        self.binary = []

        #{mode: ([offsets], return loc, instruction ptr increment)}
        self.param_modes = {0:([1,2], 3, 4), 1 : ([1], 2, 3) }
        # Mode 0 = positional, Mode 1 = immediate
    
        # {instruction: (para_mode, operation)}
        self.funct_table = {1: (0, lambda x,y: x+y), 2: (0, lambda x,y : x * y)}

    # Parses a pure binary
    def parse(self, bin):
        while self.inst_ptr <= len(bin):
            if bin[self.inst_ptr] == 99: # No op
                break

            # Instruction fetch
            inst= self.funct_table[bin[self.inst_ptr]]
            p_mode = self.param_modes[inst[0]]

            # Get the data and run the operation
            data = [bin[bin[self.inst_ptr + x]] for x in p_mode[0]]
            result = reduce(inst[1], data)

            # Store the result and increment the instruction pointer
            bin[bin[self.inst_ptr + p_mode[1]]] = result
            self.inst_ptr += p_mode[2]
    
        self.inst_ptr = 0
        return bin

    # Parses the curently set binary with a verb/noun combo
    def parse_nv(self, noun, verb):

        newbin = deepcopy(self.binary)
        newbin[1] = noun
        newbin[2] = verb
        newbin = self.parse(newbin)

        return newbin[0]
