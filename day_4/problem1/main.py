#!/usr/bin/env python3
# https://adventofcode.com/2019/day/4
num = 0
for i in range(382345,843167):
    passw = str(i)
    increment = True
    repeat = False
    for j in range(len(passw)-1):
        if int(passw[j]) > int(passw[j+1]):
            increment = False
            break
        elif int(passw[j]) == int(passw[j+1]):
            repeat = True

    if increment and repeat:
        #print(passw)
        num += 1

print(num)
